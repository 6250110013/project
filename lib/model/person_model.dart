class PersonModel {
  int? id;
  String name;
  String email;
  String image;

  PersonModel({
    this.id,
    required this.name,
    required this.email,
    required this.image,

  });



  factory PersonModel.fromMap(Map<String, dynamic> json) => new PersonModel(
        id: json['id'],
        name: json['name'],
    email: json['email'],
        image: json['image'],
      );





  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
       'email':email,
      'image': image,
    };
  }
}
