import 'package:flutter/material.dart';
import 'package:path/path.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column();
  }
}

class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('About'),
        backgroundColor: Colors.indigo,
      ),
      body: Column(
        children: [
          image(
            assetImage: AssetImage("assets/images/Meen.jpg"),
          ),
          SizedBox(height: 20),
          name(
            text: Text(
              "Sasikarn Thipbordee 6250110013  ICM",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 70),
          image(
            assetImage: AssetImage("assets/images/Non.jpg"),
          ),
          SizedBox(height: 20),
          name(
            text: Text(
              "Pongsatorn Sangkaew 6250110020  ICM",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}

class name extends StatelessWidget {
  const name({Key? key, required this.text}) : super(key: key);

  final Text text;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
      child: FlatButton(
        padding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(55)),
        color: Colors.indigo,
        onPressed: () {},
        child: Row(
          children: [
            SizedBox(width: 15),
            Expanded(
              child: text,
            ),
          ],
        ),
      ),
    );
  }
}

class image extends StatelessWidget {
  const image({Key? key, required this.assetImage}) : super(key: key);

  final AssetImage assetImage;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30),
      child: SizedBox(
        height: 140,
        width: 140,
        child: CircleAvatar(
          backgroundImage: assetImage,
        ),
      ),
    );
  }
}
