import 'dart:io';

import 'package:flutter/material.dart';
import 'package:project_sqlite/database/database_helper.dart';
import 'package:project_sqlite/model/person_model.dart';

class ShowPerson extends StatelessWidget {
  final id;

  ShowPerson({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Favorite Person'),
      ),
      body: Center(
        child: FutureBuilder<List<PersonModel>>(
            future: DatabaseHelper.instance.getProfile(this.id),
            builder: (BuildContext context,
                AsyncSnapshot<List<PersonModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No person to in List.'))
                  : ListView(
                      children: snapshot.data!.map((person) {
                        return Center(
                          child: Column(
                            children: [
                              SizedBox(
                                height: 90,
                              ),
                              CircleAvatar(
                                backgroundImage: FileImage(File(person.image)),
                                radius: 160,
                              ),
                              SizedBox(
                                height: 60,
                              ),
                              Text(
                                '${person.name}',
                                style: TextStyle(
                                    fontSize: 26, fontWeight: FontWeight.bold),
                              ),

                              SizedBox(
                                height: 20,
                              ),
                              Text('Email: ${person.email}',
                                  style: TextStyle(
                                    fontSize: 20,
                                  )),
                            ],
                          ),
                        );
                      }).toList(),
                    );
            }),
      ),
    );
  }
}
