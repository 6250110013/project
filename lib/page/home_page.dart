import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:project_sqlite/database/database_helper.dart';
import 'package:project_sqlite/model/data_model.dart';
import 'package:project_sqlite/model/person_model.dart';
import 'package:project_sqlite/page/about.dart';
import 'package:project_sqlite/page/add_person.dart';
import 'package:project_sqlite/page/edit_person.dart';
import 'package:project_sqlite/page/show_person.dart';
import 'package:project_sqlite/pages/about.dart';
import 'package:project_sqlite/pages/add_person.dart';
import 'package:project_sqlite/pages/edit_person.dart';
import 'package:project_sqlite/pages/show_person.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int? selectedId;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          IconButton(
            icon: Icon(Icons.exit_to_app_sharp),
            onPressed: () {
              SystemNavigator.pop();
            },
          )
        ],
      ),
      body: Center(
        child: FutureBuilder<List<PersonModel>>(
            future: DatabaseHelper.instance.getperson(),
            builder: (BuildContext context,
                AsyncSnapshot<List<PersonModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Favorite Person to List.'))
                  : ListView(
                      children: snapshot.data!.map((person) {
                        return Center(
                          child: Card(
                            color: selectedId == person.id
                                ? Colors.white70
                                : Colors.white10,
                            child: ListTile(
                              title: Text('${person.name} ชื่อ'),
                              subtitle: Text('${person.email}อีเมล์ '),

                              leading: CircleAvatar(
                                  backgroundImage:
                                      FileImage(File(person.image))),
                              trailing: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.edit),
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                EditPerson(person)),
                                      ).then((value) {
                                        setState(() {});
                                      });
                                    },
                                  ),
                                  new IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.clear),
                                    onPressed: () {
                                      showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            title: new Text(
                                                "Do you want to delete this record?"),
                                            // content: new Text("Please Confirm"),
                                            actions: [
                                              new TextButton(
                                                onPressed: () {
                                                  DatabaseHelper.instance
                                                      .remove(person.id!);
                                                  setState(() {
                                                    Navigator.of(context).pop();
                                                  });
                                                },
                                                child: new Text("Ok"),
                                              ),
                                              Visibility(
                                                visible: true,
                                                child: new TextButton(
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: new Text("Cancel"),
                                                ),
                                              ),
                                            ],
                                          );
                                        },
                                      );
                                    },
                                  ),
                                ],
                              ),
                              onTap: () {
                                var person;
                                var profileid =person.id;
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            ShowPerson(id: profileid)));

                                setState(() {
                                  print(person.image);
                                  if (selectedId == null) {
                                    //name.text = person.name;
                                    selectedId = person.id;
                                  } else {
                                    // textController.text = '';
                                    selectedId = null;
                                  }
                                });
                              },
                              onLongPress: () {
                                setState(() {
                                  var person;
                                  DatabaseHelper.instance.remove(person.id!);
                                });
                              },
                            ),
                          ),
                        );
                      }).toList(),
                    );
            }),
      ),
      drawer: Drawer(
        child: Container(
          color: Color.fromRGBO(50, 75, 205, 1),
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 70),
            children: <Widget>[
              const SizedBox(
                height: 16,
              ),
              buildMenuItem(
                text: 'Favorite person',
                icon: Icons.person,
                onClicked: () => selectedItem(context, 0),
              ),
              const SizedBox(
                height:18,
              ),
              buildMenuItem(
                text: 'Add Favorite person',
                icon: Icons.person_add_alt,
                onClicked: () => selectedItem(context, 1),
              ),
              const SizedBox(
                height: 16,
              ),
              buildMenuItem(
                text: 'About',
                icon: Icons.favorite,
                onClicked: () => selectedItem(context, 2),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildMenuItem({
    required String text,
    required IconData icon,
    VoidCallback? onClicked,
  }) {
    final color = Colors.white;
    final hoverColor = Colors.white70;

    return ListTile(
      leading: Icon(icon, color: color),
      title: Text(
        text,
        style: TextStyle(color: color),
      ),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }

  void selectedItem(BuildContext context, int index) {
    switch (index) {
      case 0:
        Navigator.pop(context);
        break;
      case 1:
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AddPerson()),
        );
        break;
      case 2:
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const About()),
        );
        break;
    }
  }
}
