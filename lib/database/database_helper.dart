import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:project_sqlite/model/person_model.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'db_person.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE personDB (
          id INTEGER PRIMARY KEY,
          name TEXT,
          email  TEXT,
          image TEXT
      )
      ''');
  }

  Future<List<PersonModel>> getperson() async {
    Database db = await instance.database;
    var person = await db.query('personDB', orderBy: 'name');
    List<PersonModel> personList = person.isNotEmpty
        ? person.map((c) => PersonModel.fromMap(c)).toList()
        : [];
    return personList;
  }

  Future<List<PersonModel>> getProfile(int id) async {
    Database db = await instance.database;
    var profile = await db.query('PersonDB', where: 'id = ?', whereArgs: [id]);
    List<PersonModel> profileList = profile.isNotEmpty
        ? profile.map((c) => PersonModel.fromMap(c)).toList()
        : [];
    return profileList;
  }

  Future<int> add(PersonModel person) async {
    Database db = await instance.database;
    return await db.insert('PersonDB', person.toMap());
  }

  Future<int> remove(int id) async {
    Database db = await instance.database;
    return await db.delete('PersonDB', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> update(PersonModel person) async {
    Database db = await instance.database;
    return await db.update('PersonDB', person.toMap(),
        where: "id = ?", whereArgs: [person.id]);
  }
}
