import 'package:flutter/material.dart';
import 'package:project_sqlite/page/home_page.dart';
import 'package:project_sqlite/pages/add_person.dart';
import 'package:project_sqlite/pages/home_page.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: const MyHomePage(title: 'Favorite Person'),
    );
  }
}
